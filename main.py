import argparse
from calibrate import main

if __name__ == "__main__":
    """Usage: python calibration.py [inputfile.xls] [outputfile_values.csv] [outputfile_bias.csv]"""
    print """
Calibrate with Confidence
-------------------------

This script takes an input file (either Excel or .csv):
- The first column should list the objects being assessed
- The second column should list the assessors
- The third column should give the score for that pair of object and assessor
- The fourth column should give the confidence for that pair of object and assessor

The script outputs two files: a file of values for each object, and a file of biases for each assessor.

Usage: python calibration.py [inputfile.xls] [outputfile_values.csv] [outputfile_bias.csv]

Disclaimer: The Calibrate with Confidence team and associated groups/individuals take no responsibility for the loss of data, errors in the model or its implementation or the outcomes resulting from using the model.
"""
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('-o', '--out', default="results.xlsx", help="Results output file")
    parser.add_argument('-s', '--std', action="store_true", help="Confidence values are actually standard deviations")
    parser.add_argument('-d', '--simple_degeneracy', action="store_true", help="Use simple degeneracy")
    args = parser.parse_args()

    inputf = args.input_file
    outf = args.out
    std_dev = args.std
    if args.simple_degeneracy:
        degeneracy = True
    else:
        degeneracy = False

    out = main.run(inputf, outf, std_dev=std_dev, simple_degeneracy=degeneracy, graphs=True)
    print """Confidence-weighted RMS for |dv,db| = {dv_db}""".format(dv_db=out['dv_db'])
    print "Done."
