Calibrate with Confidence
-------------------------

This repository contains the Calibrate with Confidence python package, including a script and a GUI. Both the script and the GUI require python2.7.

The script takes an input file (either Excel or .csv):

* The first column should list the objects being assessed
* The second column should list the assessors
* The third column should give the score for that pair of object and assessor
* The fourth column should give the confidence for that pair of object and assessor

The script outputs two files: a file of values for each object, and a file of biases for each assessor.

### Usage ###
```
#!python

python calibration.py [inputfile.xls] [outputfile_values.csv] [outputfile_bias.csv]
```

### Disclaimer ###
The Calibrate with Confidence team and associated groups/individuals take no responsibility for the loss of data, errors in the model or its implementation or the outcomes resulting from using the model.

### How do I get set up? ###
* Dependencies

Please install the dependencies listed in requirements.txt. Most of these can be installed using:

```
#!python

pip install -r requirements.txt
```

On Windows, the [python-xy distribution](http://python-xy.github.io/) comes pre-installed with all of these dependencies.


* Run 
```
#!python

python setup.py develop
```

### Who do I talk to? ###

* For technical problems contact info@spectraanalytics.com