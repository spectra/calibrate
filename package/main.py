from calibrate.calibration_robust import *

if __name__ == "__main__":
    """Usage: python calibration.py [inputfile.xls] [outputfile_values.csv] [outputfile_bias.csv]"""
    print """
Calibrate with Confidence
-------------------------

This script takes an input file (either Excel or .csv):
- The first column should list the objects being assessed
- The second column should list the assessors
- The third column should give the score for that pair of object and assessor
- The fourth column should give the confidence for that pair of object and assessor

The script outputs two files: a file of values for each object, and a file of biases for each assessor.

Usage: python calibration.py [inputfile.xls] [outputfile_values.csv] [outputfile_bias.csv]

Disclaimer: The Calibrate with Confidence team and associated groups/individuals take no responsibility for the loss of data, errors in the model or its implementation or the outcomes resulting from using the model.
"""
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('-o', '--out', default="results.xlsx", help="Results output file")
    parser.add_argument('-s', '--std', action="store_true", help="Confidence values are actually standard deviations")
    parser.add_argument('-d', '--simple_degeneracy', action="store_true", help="Use simple degeneracy")
    args = parser.parse_args()
    
    inputf = args.input_file
    outf = args.out
    std_dev = args.std
    if args.simple_degeneracy:
        degeneracy = 'simple'
    else:
        degeneracy = 'weighted'
    c, s, u_a, u_o = load_dummy_data(inputf,std_dev=std_dev)
    
    out = run_calibration(c,s,u_a, u_o, std_devs=std_dev, degeneracy_condition=degeneracy)

    generate_graphs(c,out['b'],out['v'])
    
    
    #outf_dv = os.path.splitext(outf_v)[0]+'_delta.csv'
    #outf_db = os.path.splitext(outf_b)[0]+'_delta.csv'
    
    writer = pd.ExcelWriter(outf)
    
    out['v'].to_excel(writer,'Objects', index=False)
    out['b'].to_excel(writer,'Assessors' ,index=False)
    #out['dv'].to_excel(outf_dv, index=False)
    #out['db'].to_excel(outf_db, index=False)
    writer.save()
    print """Confidence-weighted RMS for |dv,db| = {dv_db}""".format(dv_db=out['dv_db'])
    print "Done."
    
