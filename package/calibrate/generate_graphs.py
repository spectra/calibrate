import networkx as nx
import numpy as np
import seaborn
import pylab as plt
from matplotlib import cm

def get_nodes(b,v,c_o, c_a):
	b['c_a'] = c_a
	v['c_o'] = c_o
	b_nodes = b.to_dict('records')
	v_nodes = v.to_dict('records')
	return b_nodes, v_nodes

def get_neighbour_mat(c):
	c_conn = (c>0)
	common = c_conn.dot(1.0*c_conn.T)
	np.fill_diagonal(common,0)
	tot_c = c.dot(c.T)
	out = tot_c/common
	out[tot_c==0] = 0
	np.fill_diagonal(out,0)
	return out
    

def _node_value(node):
	if node[1]['kind'] == 'assessor':
		return node[1]['bias']
	else:
		return node[1]['value']
    
def get_node_property(G,prop):
    	return [node[1][prop] for node in G.nodes(data=True)]

def scale_property(out, xmin=300,xmax=500):
	minx = out.min()
	maxx = out.max()
	dx = maxx-minx
	dx_new = xmax-xmin
	return 1.0*(out-minx)*(1.0*dx_new/dx)+xmin
    
def get_scaled_property(G,prop,xmin=300,xmax=500):
	out = np.asarray(get_node_property(G,prop))
	return scale_property(out, xmin=xmin, xmax=xmax)

def node_values(G):
	return [_node_value(node) for node in G.nodes(data=True)]

def edge_values(G,prop='confidence'):
	out = np.asarray([edge[2][prop] for edge in G.edges(data=True)])
	return out

def shift_pos(pos,dx=0,dy=0):
	newpos = pos.copy()
	return {k:(v[0]+dx,v[1]+dy) for k,v in pos.iteritems()}


def calc_assessor_edge(c, c_o, c_a):
	N_a = c_a.shape[0]
	N_o = c_o.shape[0]
	assessor_edge = np.zeros([N_a, N_a])
	for a in range(0, N_a):
		for a_ in range(0, N_a):
			temp = 0
			for o in range(0, N_o):
				temp += c[a,o]*c[a_,o]/c_o[o]
			assessor_edge[a,a_] = temp
	return assessor_edge

def gen_assessor_graph(c, b,v,c_o, c_a, u_o, u_a):
	b_nodes, v_nodes = get_nodes(b,v,c_o, c_a)
	G_neigh = nx.Graph()
	G_neigh.add_nodes_from([(x['Assessor'],{'bias':x['Bias'],'c_a':x['c_a']}) for x in b_nodes]
			 ,kind='assessor')

	neighbour = get_neighbour_mat(c)
	assessor_edge = calc_assessor_edge(c,c_o, c_a)
	neighbour = np.triu(assessor_edge)

	for a in range(c.shape[0]):
	    for a_other in range(c.shape[0]):
		neigh = neighbour[a,a_other]
		if neigh>0:
		    G_neigh.add_edge(u_a[a],u_a[a_other],{'shared':neigh})
	return G_neigh

def gen_object_assessor_graph(c, b,v,c_o, c_a, u_o, u_a):
	b_nodes, v_nodes = get_nodes(b,v,c_o, c_a)
	assess, ob = np.where(c>=0)
	edge_list = []

	G = nx.Graph()
	G.add_nodes_from([(x['Assessor'],{'bias':x['Bias'],'c_a':x['c_a']}) for x in b_nodes]
			 ,kind='assessor')


	G.add_nodes_from([(x['Object'],{'value':x['Values'],'c_o':x['c_o']}) for x in v_nodes]
			 ,kind='object')
	for edge in zip(assess,ob):
	    conf = c[edge[0],edge[1]]
	    if conf > 0:
		new_edge = (u_a[edge[0]], u_o[edge[1]],{'confidence': conf})
		edge_list.append(new_edge)
	    
	G.add_edges_from(edge_list)
	return G

def plot_graphs(G_neigh, G):
	# import seaborn
	seaborn.set_context("talk",font_scale=1.5)
	fig = plt.figure(figsize=(12,10))
	pos = nx.circular_layout(G_neigh)
	nodes_a_vals = node_values(G_neigh)
	vrange_a = max(np.abs(nodes_a_vals))
	node_plt = nx.draw_networkx_nodes(G_neigh,pos,
			 node_size=get_scaled_property(G_neigh,'c_a',xmin=500,xmax=1000),
			node_color=nodes_a_vals,vmin=-vrange_a,vmax=vrange_a,
			                  cmap=cm.RdBu)
	edge_plt =  nx.draw_networkx_edges(G_neigh,pos,
			                   width=scale_property(edge_values(G_neigh,prop='shared'),
			                                        xmin=0.1,xmax=10),
			                   edge_color='#333333')

	dark_nodes = [node[0] for node in G_neigh.nodes(data=True) if abs(node[1]['bias'])>0.5*vrange_a]
	pos_d = {n:pos[n] for n in dark_nodes}
	light_nodes = [node[0] for node in G_neigh.nodes(data=True) if abs(node[1]['bias'])<=0.5*vrange_a]
	pos_l = {n:pos[n] for n in light_nodes}
	lbl_plt_l = nx.draw_networkx_labels(G_neigh.subgraph(light_nodes),pos_l,font_color='#000000',font_size=20)
	lbl_plt_d = nx.draw_networkx_labels(G_neigh.subgraph(dark_nodes),pos_d,font_color='#FFFFFF',font_size=20)
	_ = plt.axis('off')
	fig.subplots_adjust(right=0.8)
	cbar_ax1 = fig.add_axes([0.9,0.2,0.05,0.7])
	cb1 = fig.colorbar(node_plt,cax=cbar_ax1)
	cb1.set_label("Bias")
	cb1.solids.set_rasterized(True)
	fig.savefig("./assess_conn_new.svg",bbox_inches='tight')
	fig.savefig("./assess_conn_new.png",bbox_inches='tight')


	G_a = G.subgraph([k for k,v in G.node.items() if v['kind']=='assessor'])
	G_o = G.subgraph([k for k,v in G.node.items() if v['kind']=='object'])
	ypos_a = np.linspace(10,30,num=len(G_a))
	ypos_o = np.linspace(0,40,num=len(G_o))

	pos_a = {n:(0,ypos_a[i]) for i,n in enumerate(G_a.node)}
	pos_o = {n:(10,ypos_o[i]) for i,n in enumerate(G_o.node)}
	#pos_a.update(pos_o)
	all_pos = pos_a.copy()
	all_pos.update(pos_o)
	
	fig = plt.figure(figsize=(12,15))
	#gs = matplotlib.gridspec.GridSpec(1,3,width_ratios=[1,5,1])
	ax2 = fig.add_subplot(111)
	#ax2 = subplot(gs[1])
	#ax3 = subplot(gs[2])
	nodes_a_vals = node_values(G_a)
	vrange_a = max(np.abs(nodes_a_vals))
	nodes_a = nx.draw_networkx_nodes(G_a,ax=ax2,pos=pos_a,
		                         node_color=node_values(G_a),
		                         node_size=get_scaled_property(G_a,'c_a',xmin=200,xmax=700),
		                         cmap=cm.RdBu,vmin=-vrange_a,vmax=vrange_a,
		                        )
	nodes_o = nx.draw_networkx_nodes(G_o,ax=ax2,pos=pos_o,node_color=node_values(G_o),
		                         cmap=cm.Greens,
		                        node_size=get_scaled_property(G_o,'c_o',xmin=200,xmax=700),)
	edges = nx.draw_networkx_edges(G,ax=ax2,pos=all_pos,width=edge_values(G),edge_color='#333333')
	nx.draw_networkx_labels(G_a,shift_pos(pos_a,dx=-0.5),font_size=20)
	nx.draw_networkx_labels(G_o,shift_pos(pos_o,dx=0.5),font_size=20)

	#ax1.axis('off')
	ax2.axis('off')
	#ax3.axis('off')
	fig.subplots_adjust(left=0.07,right=0.9)
	cbar_ax1 = fig.add_axes([0.9,0.255,0.05,0.5])
	cbar_ax2 = fig.add_axes([0.025,0.255,0.05,0.5])
	cb1 = fig.colorbar(nodes_o,cax=cbar_ax1)
	cb1.set_label('Objects')
	cb1.ax.yaxis.set_label_position("left")
	cb1.solids.set_rasterized(True)
	cb2 = fig.colorbar(nodes_a,cax=cbar_ax2)
	cb2.ax.yaxis.set_ticks_position("left")
	cb2.ax.yaxis.set_label_position("right")
	cb2.set_label("Assessor Bias",)
	cb2.solids.set_rasterized(True)
	fig.subplots_adjust()
	#colorbar(nodes_o)
	fig.savefig("./assess_obj.pdf",bbox_inches='tight')
	fig.savefig("./assess_obj.svg",bbox_inches='tight')
	fig.savefig("./assess_obj.png",bbox_inches='tight')



