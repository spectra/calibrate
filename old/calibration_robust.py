import numpy as np
import sys
import pandas as pd
import os
import argparse


def load_dummy_data(filename, std_dev=False):
    """Load data from a .csv file (eventually from an excel file too...).
    Returns the confidences (c) and the score (s)."""
    ext = os.path.splitext(filename)[1]
    if ext == '.csv':
        rawdata = pd.read_csv(filename)
    else:
        rawdata = pd.read_excel(filename)
    #data['No'] = data.No.fillna(method='ffill')
    data = pd.DataFrame({'object':rawdata.iloc[:,0],'assessor':rawdata.iloc[:,1], 'score': rawdata.iloc[:,2], 'confidence':rawdata.iloc[:,3]})
    N_a = len(data.assessor.unique())
    unique_assessor = data.assessor.unique()
    assessor_dict = {unique_assessor[i]:i for i in range(N_a)}
    N_o = len(data.object.unique())
    unique_object = data.object.unique()
    object_dict = {unique_object[i]:i for i in range(N_o)}
    
    c = np.zeros([N_a, N_o])
    s = np.zeros([N_a, N_o])
    data['object'] = data.object.replace(object_dict)
    data['assessor'] = data.assessor.replace(assessor_dict)
    #print data
    lamb = 1.75
    confs = ['h','m','l']
    if any([u in confs for u in data.confidence.unique()]):
        data.confidence.replace({'l':lamb**(-2),'m':1,'h':lamb**(2)}, inplace=True)
    for i in range(data.shape[0]):
        conf = data.irow(i).confidence
        if std_dev:
            conf=1.0/conf**2
        c[data.irow(i).assessor, data.irow(i).object] = conf
        s[data.irow(i).assessor, data.irow(i).object] = data.irow(i).score
    
    #print c.shape
    #print c
    return c, s, unique_assessor, unique_object

def calc_values(c, s, degeneracy_condition='default'):
    """Calculate the values from a matrix of confidences (c) and scores (s).
    We are solving the equation L1 * b = Y"""
    N_a = c.shape[0]
    N_o = c.shape[1]
    B = np.zeros([N_a])
    for a in range(0,N_a):
        for o in range(0,N_o):
            B[a] += c[a,o]*s[a,o]

    V = np.zeros([N_o])
    for o in range(0,N_o):
        for a in range(0,N_a):
            V[o] += c[a,o]*s[a,o]

    C = np.zeros([N_o])
    for o in range(0,N_o):
        for a in range(0,N_a):
            C[o] += c[a,o]

    # L1 is the matrix/operator acting on the vector of biases (which we are solving for)
    L1 = np.zeros([N_a, N_a])
    for a in range(0,N_a-1):
        for a_ in range(0, N_a):
            temp = 0
            for o in range(0, N_o):
                temp += c[a,o]*c[a_,o]/C[o]
            L1[a,a_] = temp
        temp = 0
        for o in range(0, N_o):
            temp += c[a,o]
        L1[a,a] -= temp
    
    if degeneracy_condition=='simple':
        # The last row of this system of equations is the condition that b[0]+b[1]+b[2]+... = 0
        L1[N_a-1,:] = 1
    else:
        # The last row of this system of equations is the condition that C'[0]*b[0]+C'[0]*b[1]+C'[0]*b[2]+... = 0
        C_dash = np.zeros([N_a])
        for a_ in range(0,N_a):
            for o_ in range(0, N_o):
                C_dash[a_] += c[a_,o_]
        
            L1[N_a-1,a_] = C_dash[a_]

    # Y is the RHS of the system of equations.
    Y = np.zeros([N_a])
    for a in range(0, N_a-1):
        temp = 0
        for o in range(0,N_o):
            temp += c[a,o]*V[o]/C[o]
        Y[a] = temp - B[a]
    
    # Again, the last row of the system is that the right hand side = 0
    Y[N_a-1] = 0
    
    # Find some way of solving L1 * b = Y. Excel has a matrix inversion command.
    b = np.linalg.solve(L1, Y)
    
    # From the vector of biases, calculate v (the vector of values).
    v = np.zeros([N_o])
    for o in range(0, N_o):
        temp = 0
        for a in range(0, N_a):
            temp += b[a]*c[a,o]
        v[o] = (V[o]-temp)/C[o]

    return b, v
    
def forward_model(c):
    """From a confidence matrix, c, generate some random data (values, biases and scores) to test on."""
    N_a = c.shape[0]
    N_o = c.shape[1]
    
    # Values are randomly generated
    v = np.random.randn(N_o) + 10
    
    # Biases are randomly generated, and add up to 0 (they don't have to, 
    # but this helps when comparing to the solution)
    b = np.random.randn(N_a)
    b[0] = -b[1:].sum()
    
    # Noise is a standard normal
    eta = np.random.randn(N_a, N_o)
    
    s = np.zeros([N_a, N_o])
    for a in range(0, N_a):
        for o in range(0, N_o):
            if c[a, o]>0.0001:
                s[a, o] = v[o] + b[a] + eta[a,o]/np.sqrt(c[a,o])

    return v,b,s

def calc_Co_Ca(c):
    """Given confidence matrix `c`, return:
    - `C_o`, the total confidence expressed by assessor `a`
    - `C_a`, the total confidence in the assessment of object `o`"""
    N_a = c.shape[0]
    N_o = c.shape[1]
    C_o = np.zeros((N_o,))
    C_a = np.zeros((N_a,))
    for o in range(N_o):
        for a in range(N_a):
            C_o[o] += c[a,o]
            C_a[a] += c[a,o]
    return C_o, C_a

def calc_mu_2(c, C_o, C_a):
    """Calculate second-smallest eigenvalue for robustness"""
    N_a = c.shape[0]
    N_o = c.shape[1]
    
    N_m = N_a + N_o
    M = np.zeros((N_m, N_m))
    for i in range(N_m):
        M[i,i] = 1
        
    DT = np.zeros((N_a,N_o))
    
    for a in range(N_a):
        for o in range(N_o):
            DT[a,o] = c[a,o] / np.sqrt(C_o[o]*C_a[a])
    
    #D = np.transpose(DT)
        
    for a in range(N_a):
        for o in range(N_o):
            M[a+N_o,o] = DT[a,o]
            M[o,a+N_o] = DT[a,o]
    eigs = np.linalg.eigvals(M)
    eigs_sorted = np.sort(eigs)
    return eigs_sorted[1]

def calc_deltas(c, ds, degeneracy_condition='default'):
    """Calculate upper limits for the change in magnitude of the values and biases vectors,
    given changes in the scores `ds`"""
    N_a, N_o = c.shape
    C_o, C_a = calc_Co_Ca(c)
    total_ds = 0.0
    for a in range(N_a):
        for o in range(N_o):
            total_ds += c[a,o]*(ds[a,o])**2
    mu_2 = calc_mu_2(c, C_o, C_a)
    
    
    if degeneracy_condition=='simple':
        degen_factor = 1.0
    else:
        degen_factor = np.sqrt(2)
        
    
    dv = np.zeros((N_o,))
    for o in range(N_o):
        dv[o] = degen_factor*np.sqrt(total_ds)/np.sqrt(mu_2*C_o[o])
    
    db = np.zeros((N_a,))
    for a in range(N_a):
        db[a] = degen_factor*np.sqrt(total_ds)/np.sqrt(mu_2*C_a[a])
    return db, dv
    
def run_calibration(c,s,u_a,u_o):
    """Run a full calibration, with robustness."""
    b,v = calc_values(c,s)
    N_a, N_o = c.shape
    ds = np.ones((N_a, N_o))*1.0/np.sqrt(c.max())*0.01
    #ds = np.ones((N_a, N_o))*1.0*c.max()*0.01
    
    db, dv = calc_deltas(c, ds)
    out_v = pd.DataFrame({'Object':u_o,'Values':v})[['Object','Values']]
    out_b = pd.DataFrame({'Assessor':u_a,'Bias':b})[['Assessor','Bias']]
    out_dv = pd.DataFrame({'Object':u_o,'Value Robustness':dv})[['Object','Value Robustness']]
    out_db = pd.DataFrame({'Assessor':u_a,'Bias Robustness':db})[['Assessor','Bias Robustness']]
    
    out = {'v':out_v, 'b':out_b,
            'dv':out_dv, 'db':out_db}
    return out

def generate_graphs(c,b,v):
    import generate_graphs as gg
    C_o, C_a = calc_Co_Ca(c)
    gg.gen_assessor_graph(c, b, v, C_o, C_a)
