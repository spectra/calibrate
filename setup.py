import py2exe
import matplotlib
from setuptools import setup

py2exe_options = dict(compressed=False, bundle_files=1,
                        dll_excludes=['w9xpopen.exe'],
                        excludes=['doctest','_ssl','pdb','matplotlib','scipy',
                      'PyQt4','statsmodels','PIL','tables','Tkconstants',
                      'Tkinter'])

setup(
    name='calibrate',
    version = '0.1.0',
    packages = ['calibrate'],
    windows=[{'script':"run_gui.py"}],
    zipfile=None
)
