import wx

from calibrate import gui

if __name__ == '__main__':
    app = wx.App(False)
    gui.MainFrame(None, title='Calibrate with Confidence')
    app.MainLoop()
