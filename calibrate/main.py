from calibrate.calibration_robust import *
import os

def run(inputf, outf, std_dev=False, simple_degeneracy=False, graphs=False, lamb=1.75):

    if simple_degeneracy:
        print "Using simple degeneracy-breaking condition"
        degeneracy = 'simple'
    else:
        print "Using weighted degeneracy-breaking condition"
        degeneracy = 'weighted'

    try:
        c, s, u_a, u_o = load_dummy_data(inputf,std_dev=std_dev, lamb=lamb)
        try:
            out = run_calibration(c,s,u_a, u_o, std_devs=std_dev, degeneracy_condition=degeneracy)
            if graphs:
                try:
                    filename, extension = os.path.splitext(outf)
                    generate_graphs(c,out['b'],out['v'], basefile=filename)
                except Exception as ie:
                    out = {'error':ie, 'msg':'Error generating graphs. Please try without graph generation'}
        except Exception as ie:
            out = {'error':ie, 'msg':'Error running calibration.'}
    except Exception as ie:
        out = {'error':ie, 'msg':'Error loading data. Is it in the correct format?'}

    #outf_dv = os.path.splitext(outf_v)[0]+'_delta.csv'
    #outf_db = os.path.splitext(outf_b)[0]+'_delta.csv'
    if 'error' not in out:
        writer = pd.ExcelWriter(outf)
        out['v'].to_excel(writer,'Objects', index=False)
        out['b'].to_excel(writer,'Assessors' ,index=False)
        #out['dv'].to_excel(outf_dv, index=False)
        #out['db'].to_excel(outf_db, index=False)
        writer.save()
    return out
