import wx
import os

from threading import Thread

from calibrate import main, config

ID_RUNNER = wx.NewId()

myEVT_COUNT = wx.NewEventType()
EVT_COUNT = wx.PyEventBinder(myEVT_COUNT, 1)
class CountEvent(wx.PyCommandEvent):
    """Event to signal that a count value is ready"""
    def __init__(self, etype, eid, value=None):
        """Creates the event object"""
        wx.PyCommandEvent.__init__(self, etype, eid)
        self._value = value
    def GetValue(self):
        """Returns the value from the event.
        @return: the value of this event
        """
        return self._value

class RunnerThread(Thread):
    """Test Worker Thread Class."""

    #----------------------------------------------------------------------
    def __init__(self, parent,**kwargs):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self._parent = parent
        self.data = kwargs
        self.start()    # start the thread

    #----------------------------------------------------------------------
    def run(self):
        """Run Worker Thread."""
        outdata = main.run(**self.data)
        self.outdata = outdata
        evt = CountEvent(myEVT_COUNT, -1, outdata)
        wx.PostEvent(self._parent, evt)
        #wx.CallAfter(Publisher().sendMessage, ("runner"), (outdata, logger) )
        # This is the code executing in the new thread.
        #for i in range(20):
        #    time.sleep(1)
        #    wx.CallAfter(pub.sendMessage, "update", msg="")
    def get_result(self):
        if self.thread is None:
            return None
        else:
            if self.thread.isAlive():
                return None
            else:
                return self.outdata, self.logger



class WarningsDialog(wx.Dialog):
    """Display Logger warnings in a dialog"""

    def __init__(self, parent, title, logger=None):
        style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER
        #newkw = {k:v for k, v in kw.iteritems() if k!='logger'}
        self.logger = logger

        super(WarningsDialog, self).__init__(parent, -1,title, style=style)
        self.InitUI()
        self.SetSize((700,300))
        self.SetTitle("Warnings")
        self.should_save = False


    def InitUI(self):
        panel = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)
        self.lstctrl_warns = wx.ListCtrl(panel, style=wx.LC_REPORT|wx.BORDER_SUNKEN)
        self.lstctrl_warns.InsertColumn(0, 'Warning')
        self.lstctrl_warns.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        #self.lstctrl_warns.InsertColumn(1, 'Number')
        #self.lstctrl_warns.SetColumnWidth(0,100)

        vbox.Add(self.lstctrl_warns, 2, wx.EXPAND|wx.ALL, 10)
        for i, l in enumerate(self.logger.logs):
            self.lstctrl_warns.InsertStringItem(i, l)
            #self.lstctrl_warns.SetItem(i, 1, self.logger.logs[l])
        self.lstctrl_warns.SetColumnWidth(0, wx.LIST_AUTOSIZE)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        cmd_ok = wx.Button(panel, wx.ID_OK, label="Save anyway")
        cmd_ok.Bind(wx.EVT_BUTTON, self.OnOK)
        hbox.Add(cmd_ok, 0, wx.ALL, 10)
        cmd_cancel = wx.Button(panel, wx.ID_CANCEL, label="Cancel")
        cmd_cancel.Bind(wx.EVT_BUTTON, self.OnClose)
        hbox.Add(cmd_cancel, 0, wx.ALL, 10)
        vbox.Add(hbox, 0, wx.EXPAND)
        panel.SetSizer(vbox)
        vbox.Fit(panel)

    def OnOK(self, e):
        self.should_save = True
        self.EndModal(wx.ID_OK)
        self.Destroy()

    def OnClose(self, e):
        self.EndModal(wx.ID_CANCEL)
        self.Destroy()


class MainFrame(wx.Frame):
    def __init__(self, parent, title):
        super(MainFrame, self).__init__(parent, title=title,
            size=(600, 200))
        self.dirname = None
        self.filename = None
        self.abspath = None
        self.output_file = config.settings['default_output_file']

        self.file_id = 0
        self.times_file = config.settings['default_times_file']
        self.inputfile = ""
        self.outputfile = ""
        self.std_dev = False
        self.simple_degeneracy = False
        self.graphs = False
        self.InitUI()
        self.Centre()
        self.Show()

    def InitUI(self):
        panel = wx.Panel(self)
        self.grid = []
        box = wx.BoxSizer(wx.VERTICAL)
        box_row = wx.BoxSizer(wx.HORIZONTAL)
        box_times = wx.BoxSizer(wx.HORIZONTAL)

        self.displays = {'save':wx.TextCtrl(panel),
                        'open':wx.TextCtrl(panel)}

        file_boxes = [{'display':self.displays['open'],
                            'button':wx.Button(panel, wx.ID_OPEN, label="Choose input file..." ),
                            'onclick':self.OnOpen, 'output':self.inputfile},
                            {'display':self.displays['save'],
                            'button':wx.Button(panel, wx.ID_OPEN, label="Save output to..." ),
                            'onclick':self.OnSave, 'output':self.outputfile}]

        self.radioboxes = {'simple_degeneracy':wx.RadioBox(panel, label='Degeneracy-breaking condition', choices=['Simple','Weighted']),
                        'std_dev': wx.RadioBox(panel, label='Confidence specified as...', choices=['Standard deviations','Confidences']),
                        'graphs':wx.CheckBox(panel, label='Generate graphs'),
                        'lamb':wx.TextCtrl(panel, value='1.75')
                        }
        for cb in ['simple_degeneracy','std_dev','graphs']:
            box_cb = wx.BoxSizer(wx.HORIZONTAL)
            box_cb.Add(self.radioboxes[cb], 0, wx.ALL, 10)
            box.Add(box_cb, 0, flag=wx.EXPAND)

        box_cb = wx.BoxSizer(wx.HORIZONTAL)
        box_cb.Add(wx.StaticText(panel, -1, "Lambda: "), 0, wx.ALL, 10)
        box_cb.Add(self.radioboxes['lamb'], 0, wx.ALL, 10)
        box.Add(box_cb, 0, flag=wx.EXPAND)

        for item in file_boxes:
            box_times = wx.BoxSizer(wx.HORIZONTAL)
            box_times.Add(item['button'], 0, wx.ALL, 10)
            onclick = item.get('onclick', None)
            if onclick is not None:
                item['button'].Bind(wx.EVT_BUTTON, onclick)
            item['display'].SetEditable(False)
            item['display'].SetValue(item['output'])
            box_times.Add(item['display'], 1, wx.ALL, 10)
            box_times.Fit(panel)
            box.Add(box_times, 0, flag=wx.EXPAND)

        self.cmd_open = wx.Button(panel, wx.ID_OPEN, label='Calibrate')
        self.cmd_open.Bind(wx.EVT_BUTTON, self.OnRun)
        box_row.Add(self.cmd_open, 1, wx.ALL, 10)

        self.Bind(EVT_COUNT, self.OnFinished)



        box_row.Fit(panel)
        #box.Add(box_save, 0,flag = wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP)
        box.Add(box_row,0, flag= wx.EXPAND)

        panel.SetSizer(box)
        box.Fit(panel)
        panel.Layout()
        self.Fit()
        minsize = self.GetSize()
        self.SetMinSize(minsize)
        self.Show()

    def OnResize(self, e):
        self.Fit()

    def show_file_dialog(self, title, default_path, filetypes, flag=wx.SAVE):
        dlg = wx.FileDialog(self, title, default_path, "",filetypes, flag)
        if dlg.ShowModal() == wx.ID_OK:
            val = dlg.GetPath()
        else:
            val = None
        dlg.Destroy()
        return val

    def OnSave(self, e):
        """Set save location"""
        default_path = os.path.join(os.path.expanduser(os.path.join('~','Desktop')), 'output')
        filetypes = "Excel-readable files|*.xls;*.XLS;*.xlsx;*.XLSX"
        self.outputfile = self.show_file_dialog('Save output to...', default_path, filetypes)
        if self.outputfile is not None:
            split = os.path.splitext(self.outputfile)
            if split[1] not in ['.xlsx','.XLS','.XLSX', '.xls']:
                self.outputfile = split[0] + '.xlsx'
        self.displays['save'].SetValue(self.outputfile)

    def OnOpen(self, e):
        """ Open a file"""
        default_path = os.path.join(os.path.expanduser(os.path.join('~','Desktop')), 'output')
        filetypes = "Excel-readable files|*.csv;*.xls;*.XLS;*.xlsx;*.XLSX"
        self.inputfile = self.show_file_dialog('Open input file...', default_path, filetypes, flag=wx.OPEN)
        self.displays['open'].SetValue(self.inputfile)


    def parse_ctl(self, ctl):
        try:
            return ctl.GetStringSelection().lower()
        except AttributeError:
            return ctl.GetValue()

    def OnRun(self, e):
        opts = {k:self.parse_ctl(v) for k, v in self.radioboxes.iteritems()}
        if opts['std_dev']=='standard deviations':
            opts['std_dev'] = True
        else:
            opts['std_dev'] = False
        if opts['std_dev'] == 'simple':
            opts['simple_degeneracy']=True
        else:
            opts['simple_degeneracy']=False
        opts['lamb'] = self.radioboxes['lamb'].GetValue()
        if len(opts['lamb'])==0 and not opts['std_dev']:
            wx.MessageBox('Confidences require a value for lambda, but no value was given.', 'Error',
                          wx.OK | wx.ICON_ERROR)
            return
        else:
            try:
                if len(opts['lamb'])==0:
                    opts['lamb'] = 0
                opts['lamb'] = float(opts['lamb'])
                if not 1<=opts['lamb']:
                    wx.MessageBox('Value given for lambda is less than 1', 'Error',
                                  wx.OK | wx.ICON_ERROR)
                    return
            except ValueError:
                wx.MessageBox('Value given for lambda is not a real number', 'Error',
                              wx.OK | wx.ICON_ERROR)
                return
        if len(self.inputfile)>0:
            rt = RunnerThread(self, inputf=self.inputfile, outf=self.outputfile,
                                **opts)
            self.cmd_open.Disable()
            self.cmd_open.SetLabel("Calibrating...")
        else:
            wx.MessageBox('No input files selected.', 'Error',
                          wx.OK | wx.ICON_ERROR)
            return

    def OnFinished(self, e):
        outdata = e.GetValue()
        if 'error' not in outdata:
            msg ="Saved data to {filename}.\n\nConfidence-weighted RMS for |dv,db| = {dv_db}".format(filename=self.outputfile,
                                                                                        dv_db=outdata['dv_db'])
            wx.MessageBox(msg, 'Saved', wx.OK|wx.ICON_INFORMATION)
        else:
            msg = "Error: {msg}\n\nOriginal error was: {e}".format(msg=outdata['msg'], e=outdata['error'])
            wx.MessageBox(msg, 'Saved', wx.OK|wx.ICON_ERROR)
        self.cmd_open.Enable()
        self.cmd_open.SetLabel("Calibrate")
