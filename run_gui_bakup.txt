# -*- mode: python -*-
a = Analysis(['.\\run_gui.py'],
             pathex=['C:\\Users\\daniel\\Documents\\calibrate'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)

# remove lib2to3
#targets = []
#for module in a.pure:
#    if module[0].startswith('lib2to3'):
#        targets.append(module)

#for target in targets:
#    a.pure.remove(target)

pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='run_gui.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='run_gui')
